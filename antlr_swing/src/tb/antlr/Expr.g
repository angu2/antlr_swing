grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!;
    
blok
    : BEGIN^ (stat | blok)* END!;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_reg NL -> if_reg
    | while_loop NL -> while_loop
    | dowhile_loop NL -> dowhile_loop
    | for_loop NL -> for_loop

    | NL ->
    ;

if_reg
    : IF^ expr THEN! (expr) (ELSE! (expr))?
    ;
    
while_loop
    : WHILE^ expr THEN! expr
    ;
    
dowhile_loop
    : DO^ expr WHILE! expr
    ;
    
for_loop
    : FOR^ expr DO! expr    //for x times do something
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

BEGIN : '{';
END   : '}';

IF    : 'if';
THEN  : 'then';
ELSE  : 'else';

WHILE : 'while';
DO    : 'do';
FOR   : 'for';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	

