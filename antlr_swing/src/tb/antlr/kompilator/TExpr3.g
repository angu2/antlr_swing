tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (b+=bl | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d}) 
;

bl  : ^(BEGIN {enterScope();} (b+=bl | e+=expr | d+=decl)* {leaveScope();}) -> blok(wyr={$e},dekl={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) 
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID                       -> id(n={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;}   -> if(e1={$e1.st},e2={$e2.st},e3={$e3.st},n={numer.toString()})
        | ^(WHILE e1=expr e2=expr) {numer++;} -> while(e1={$e1.st},e2={$e2.st},n={numer.toString()})
        | ^(DO e1=expr e2=expr) {numer++;} -> do(e1={$e1.st},e2={$e2.st},n={numer.toString()})
        | ^(FOR e1=expr e2=expr) {numer++;} -> for(e1={$e1.st},e2={$e2.st},n={numer.toString()})
    ;
    